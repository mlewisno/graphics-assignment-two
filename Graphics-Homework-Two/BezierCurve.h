//
//  BezierCurve.h
//  Graphics-Homework-Two
//
//  Created by Mischa Lewis-Norelle on 10/14/14.
//  Copyright (c) 2014 MischaCo. All rights reserved.
//

#ifndef Graphics_Homework_Two_BezierCurve_h
#define Graphics_Homework_Two_BezierCurve_h

#pragma once
#include "float2.h"
#include "Freeform.h"

class BezierCurve : public Freeform
{
    static double bernstein(int i, int n, double t) {
        if(n == 1) {
            if(i == 0) return 1-t;
            if(i == 1) return t;
            return 0;
        }
        if(i < 0 || i > n) return 0;
        return (1 - t) * bernstein(i,   n-1, t)
        +      t  * bernstein(i-1, n-1, t);
    }
    
    float2 getPoint(float t)
    {
        float2 r(0.0, 0.0);
        float2 newPoint = float2(0, 0);
        int size = (int)controlPoints.size();
        
        for (int i = 0; i < size; i ++){
            double weight = bernstein(i, size - 1, t);
            
            r = controlPoints[i];
            r = r * weight;
            
            newPoint += r;
        }
        
        return newPoint;
    }
    
    
    float2 getDerivative(float t){
        return float2(0.0, 0.0);
    }
    
    float2 getSecondDerivative(float t){
        return float2(0.0, 0.0);
        
    }
    
    void removeControlPoint(int index){
        controlPoints.erase(controlPoints.begin() + index);
    }
    
};


#endif
