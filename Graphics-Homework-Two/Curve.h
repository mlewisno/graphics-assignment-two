#ifndef Graphics_Homework_Two_Curve_h
#define Graphics_Homework_Two_Curve_h

#pragma once
#include "float2.h"
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>

float PRECISION = 0.01;

class Curve
{
    virtual float2 getPoint(float t)=0;
    virtual float2 getDerivative(float t)=0;
    virtual float2 getSecondDerivative(float t)=0;
    
public:
    void draw(){
        glBegin(GL_LINE_STRIP);
        for (double i = 0.0; i < 1.0; i += PRECISION){
            float2 tempPoint = getPoint(i);
            glVertex2d(tempPoint.x, tempPoint.y);
        }
        glEnd();
    }
    
    void drawTracker(double t){
        glBegin(GL_POINTS);
        float2 tempPoint = getPoint(t);
        glVertex2f(tempPoint.x, tempPoint.y);
        glEnd();
        glBegin(GL_LINES);
        float2 tempDirPoint = getDerivative(t);
        glVertex2f(tempPoint.x, tempPoint.y);
        glVertex2f(tempPoint.x + tempDirPoint.x, tempPoint.y + tempDirPoint.y);
        glEnd();
        glBegin(GL_LINES);
        float2 tempSecondDirPoint = getSecondDerivative(t);
        glVertex2f(tempPoint.x, tempPoint.y);
        glVertex2f(tempPoint.x + tempSecondDirPoint.x, tempPoint.y + tempSecondDirPoint.y);
        glEnd();
        
    }
    
};


#endif
