#ifndef Graphics_Homework_Two_Circle_h
#define Graphics_Homework_Two_Circle_h

#pragma once
#include "float2.h"
#include "Curve.h"

class Circle: public Curve{
public:
    float2 center;
    float radius;
    
    Circle(float2 center, float radius){
        this->center = center;
        this->radius = radius;
    }
    
    float2 getPoint(float t){
        float time = t * 2 * M_PI;
        return float2(center.x + radius*cosf(time), center.y + radius*sinf(time));
    }
    
    float2 getDerivative(float t){
        float time = t * 2 * M_PI;
        return float2(center.x + radius*-sinf(time), center.y + radius*cosf(time));
    }
    
    float2 getSecondDerivative(float t){
        float time = t * 2 * M_PI;
        return float2(center.x + radius*-cosf(time), center.y + radius*-sinf(time));
    }
};

#endif
