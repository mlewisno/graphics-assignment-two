#ifndef Graphics_Homework_Two_Polyline_h
#define Graphics_Homework_Two_Polyline_h

#pragma once
#include "float2.h"
#include "Freeform.h"

class Polyline: public Freeform
{
    float2 getPoint(float t)
    {
        float size = (float)controlPoints.size();
        float currentSegment = 0;
        
        for (float i = 0; i < size; i++){
            if (t * (size - 1) <= i + 1 && t * (size - 1) >= i){
                currentSegment = i;
            }
        }
        
        float2 firstPoint = controlPoints[currentSegment];
        float2 secondPoint = controlPoints[currentSegment + 1];
        
        float basedTime = t - (currentSegment * (1.0f / (size - 1)));
        float scaledTime = basedTime * (size - 1);
        
        float2 newUnscaledPoint = secondPoint - firstPoint;
        float2 newScaledPoint = float2((newUnscaledPoint.x * scaledTime), (newUnscaledPoint.y * scaledTime));
        float2 newPoint = firstPoint + newScaledPoint;
        
        return newPoint;
    }
    
    float2 getDerivative(float t){
        return float2(0.0, 0.0);
    }
    
    float2 getSecondDerivative(float t){
        return float2(0.0, 0.0);
        
    }
    
    void removeControlPoint(int index){
        controlPoints.erase(controlPoints.begin() + index);
    }

    
};

#endif
