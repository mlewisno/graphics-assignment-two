#ifndef Graphics_Homework_Two_CurveInstance_h
#define Graphics_Homework_Two_CurveInstance_h
#pragma once
#include "float2.h"
#include "Curve.h"

class CurveInstance
{
protected:
    Curve* curve; // the geometric model,
    // can point to any Curve subclass
    float2 position; // instance position
public:
    CurveInstance(Curve* curve, float2 position)
    :curve(curve), position(position) {}
    void draw()
    {
        glTranslatef(position.x, position.y, 0);
        curve->draw();
        glTranslatef(-position.x, -position.y, 0);
    }
    void translatePosition(float2 positionChange)
    {
        position.x += positionChange.x;
        position.y += positionChange.y;
    }
};


#endif
