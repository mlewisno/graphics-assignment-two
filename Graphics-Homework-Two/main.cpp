
#define _USE_MATH_DEFINES
#include <math.h>
#include <cmath>
#include <stdlib.h>

#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>

#include <vector>

#include "float2.h"
#include "Curve.h"
#include "CurveInstance.h"
#include "Circle.h"
#include "Heart.h"
#include "Freeform.h"
#include "FreeformInstance.h"
#include "BezierCurve.h"
#include "LagrangeCurve.h"
#include "Polyline.h"
#include "BezierNumber.h"


bool pPressed = false;
bool lPressed = false;
bool bPressed = false;

bool aPressed = false;
bool dPressed = false;

bool fPressed = false;

float precision = 0.01;
double currentTime = 0.0;

float2 oldMousePoint = float2();
float2 newMousePoint = float2();

float oldTime = 0.0f;
float newTime = 0.0f;

BezierNumber counterSecondOnesDigit = BezierNumber(0, 1.0f/4000.0f);
BezierNumber counterSecondTensDigit = BezierNumber(0, 1.0f/4000.0f);

std::vector<FreeformInstance*> freeformInstances;

int lastCurveIndex = 0;
int currentCurveIndex = 0;
int currentControlPointIndex = -1;

void addNewCurve(){
    if (freeformInstances[lastCurveIndex]->getNumberOfControlPoints() < 2){
        freeformInstances.pop_back();
    }
    freeformInstances.push_back(new FreeformInstance(new BezierCurve, *new float2));
    lastCurveIndex = freeformInstances.size() - 1;
}

float distance(float2 pointOne, float2 pointTwo){
    
    float xDistance = fabs(pointOne.x - pointTwo.x);
    float yDistance = fabs(pointOne.y - pointTwo.y);
    
    return sqrt(xDistance * xDistance + yDistance * yDistance);
}


void onDisplay(){
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    
    float elapsedTime = glutGet(GLUT_ELAPSED_TIME)/1000.0;
    
    newTime = elapsedTime;
    
    int flooredTime = floor(elapsedTime);
    int currentValue = flooredTime % 10;
    float deltaTime = newTime - oldTime;
    
    BezierNumber counterOnesDigit = BezierNumber((freeformInstances.size() - 1) % 10, 1.0f/4000.0f);
    BezierNumber counterTensDigit = BezierNumber(((freeformInstances.size() - 1) / 10) % 10, 1.0f/4000.0f);

    // The counter
    glColor3d(1.0, 1.0, 1.0);
    glTranslatef(0.7, 0.9, 0);
    counterOnesDigit.draw(deltaTime, currentValue, 1, true);
    glTranslatef(-0.7, -0.9, 0);
    glColor3d(0.0, 1.0, 0.0);
    glColor3d(1.0, 1.0, 1.0);
    glTranslatef(0.6, 0.9, 0);
    counterTensDigit.draw(deltaTime, currentValue, 1, true);
    glTranslatef(-0.6, -0.9, 0);
    glColor3d(0.0, 1.0, 0.0);
    
    // The clock
    time_t result = time(nullptr);
    int second = localtime(&result)->tm_sec;
    int minute = localtime(&result)->tm_min;
    int hour = localtime(&result)->tm_hour;
    
    /*
    BezierNumber counterHourOnesDigit = BezierNumber(hour / 10, 1.0f/4000.0f);
    BezierNumber counterHourTensDigit = BezierNumber(hour % 10, 1.0f/4000.0f);
    glColor3d(1.0, 1.0, 1.0);
    glTranslatef(-0.9, 0.9, 0);
    counterHourOnesDigit.draw(deltaTime, hour / 10, true);
    glTranslatef(0.9, -0.9, 0);
    glColor3d(0.0, 1.0, 0.0);
    glColor3d(1.0, 1.0, 1.0);
    glTranslatef(-0.8, 0.9, 0);
    counterHourTensDigit.draw(deltaTime, hour % 10, true);
    glTranslatef(0.8, -0.9, 0);
    glColor3d(0.0, 1.0, 0.0);
    
    BezierNumber counterMinuteOnesDigit = BezierNumber(minute / 10, 1.0f/4000.0f);
    BezierNumber counterMinuteTensDigit = BezierNumber(minute % 10, 1.0f/4000.0f);
    glColor3d(1.0, 1.0, 1.0);
    glTranslatef(-0.6, 0.9, 0);
    counterMinuteOnesDigit.draw(deltaTime, minute / 10, true);
    glTranslatef(0.6, -0.9, 0);
    glColor3d(0.0, 1.0, 0.0);
    glColor3d(1.0, 1.0, 1.0);
    glTranslatef(-0.5, 0.9, 0);
    counterMinuteTensDigit.draw(deltaTime, minute % 10, true);
    glTranslatef(0.5, -0.9, 0);
    glColor3d(0.0, 1.0, 0.0);
    */
    

    glColor3d(1.0, 1.0, 1.0);
    glTranslatef(-0.3, 0.9, 0);
    counterSecondTensDigit.draw(deltaTime, newTime, 10.0f, true);
    glTranslatef(0.3, -0.9, 0);
    glColor3d(0.0, 1.0, 0.0);
    glColor3d(1.0, 1.0, 1.0);
    glTranslatef(-0.2, 0.9, 0);
    counterSecondOnesDigit.draw(deltaTime, newTime, 1.0f, true);
    glTranslatef(0.2, -0.9, 0);
    glColor3d(0.0, 1.0, 0.0);

    for (int i = 0; i < freeformInstances.size(); i++) {
        if (freeformInstances[i]->getNumberOfControlPoints() > 1){
            if (i == currentCurveIndex) {
                glLineWidth(6.0);
                glColor3d(0.0, 0, 1.0);
            }
            else if (typeid(*freeformInstances[i]->getCurvePointer()) == typeid(LagrangeCurve)) {
                glColor3d(1.0, 0, 1.0);
            }
            else if (typeid(*freeformInstances[i]->getCurvePointer()) == typeid(BezierCurve)) {
                glColor3d(0.5, 0.5, 0);
            }
            else if (typeid(*freeformInstances[i]->getCurvePointer()) == typeid(Polyline)) {
                glColor3d(1.0, 0.3, 0);
            }
            freeformInstances[i]->draw();
        }
        if (freeformInstances[i]->getNumberOfControlPoints() > 1 || i == freeformInstances.size() - 1){
            glColor3d(0.0, 1.0, 0.0);
            glLineWidth(2.0);
            if (i == currentCurveIndex) {
                freeformInstances[i]->drawControlPoints(currentControlPointIndex);
            }
            else {
                freeformInstances[i]->drawControlPoints(-1);
            }
        }
        
    }
    glutPostRedisplay();
    glutSwapBuffers();
    
    oldTime = newTime;
}


void onKeyUp(unsigned char key,
             int x, int y){
    if (key == ' '){
        currentCurveIndex++;
        currentCurveIndex = currentCurveIndex % (freeformInstances.size() - 1);
        while (freeformInstances[currentCurveIndex]->getNumberOfControlPoints() < 2){
            currentCurveIndex ++;
            currentCurveIndex = currentCurveIndex % (freeformInstances.size() - 1);
        }
    }
    else if (key == 'p'){
        addNewCurve();
        pPressed = false;
    }
    else if (key == 'l'){
        addNewCurve();
        lPressed = false;
    }
    else if (key == 'b'){
        addNewCurve();
        bPressed = false;
    }
    else if (key == 'a'){
        aPressed = false;
    }
    else if (key == 'd'){
        dPressed = false;
    }
    else if (key == 'f'){
        fPressed = false;
    }
}


void onKeyDown(unsigned char key,
             int x, int y){
    if (key == 'p'){
        pPressed = true;
        lPressed = false;
        bPressed = false;
    }
    else if (key == 'l'){
        pPressed = false;
        lPressed = true;
        bPressed = false;
    }
    else if (key == 'b'){
        pPressed = false;
        lPressed = false;
        bPressed = true;
    }
    else if (key == 'a'){
        aPressed = true;
        dPressed = false;
        pPressed = false;
        lPressed = false;
        bPressed = false;
    }
    else if (key == 'd'){
        aPressed = false;
        dPressed = true;
        pPressed = false;
        lPressed = false;
        bPressed = false;
    }
    else if (key == 'f'){
        fPressed = true;
        freeformInstances[currentCurveIndex]->updatePolygonStatus(true);
    }
}


void onMouseMove(int x, int y){
    
    int windowHeight = glutGet(GLUT_WINDOW_HEIGHT);
    int windowWidth = glutGet(GLUT_WINDOW_WIDTH);
    
    double scaledX = (x - windowWidth / 2.0) / (windowWidth / 2.0);
    double scaledY = -1 * (y - windowHeight / 2.0) / (windowHeight / 2.0);
    
    float2 mousePoint = float2(scaledX, scaledY);
    
    if(currentControlPointIndex > -1){
        freeformInstances[currentCurveIndex]->updateControlPoint(currentControlPointIndex, mousePoint);
    }
    else {
        newMousePoint = mousePoint;
        float2 offSet = newMousePoint - oldMousePoint;
        freeformInstances[currentCurveIndex]->translatePosition(offSet);
        oldMousePoint = newMousePoint;
    }
}


void onMouseClick(int button, int state, int x, int y){
    
    int windowHeight = glutGet(GLUT_WINDOW_HEIGHT);
    int windowWidth = glutGet(GLUT_WINDOW_WIDTH);
    
    double scaledX = (x - windowWidth / 2.0) / (windowWidth / 2.0);
    double scaledY = -1 * (y - windowHeight / 2.0) / (windowHeight / 2.0);
    
    float2 mousePoint = float2(scaledX, scaledY);
    
    if (state == GLUT_DOWN){
        
        oldMousePoint = mousePoint;
        
        if (pPressed){
            if (typeid(*freeformInstances[lastCurveIndex]->getCurvePointer()) != typeid(Polyline)){
                freeformInstances.pop_back();
                freeformInstances.push_back(new FreeformInstance(new Polyline, *new float2));
            }
            freeformInstances[lastCurveIndex]->addControlPoint(mousePoint);
            currentCurveIndex = freeformInstances.size() - 1;
        }
        else if (lPressed){
            if (typeid(*freeformInstances[lastCurveIndex]->getCurvePointer()) != typeid(LagrangeCurve)){
                freeformInstances.pop_back();
                freeformInstances.push_back(new FreeformInstance(new LagrangeCurve, *new float2));            }
            freeformInstances[lastCurveIndex]->addControlPoint(mousePoint);
            currentCurveIndex = freeformInstances.size() - 1;
        }
        else if (bPressed){
            if (typeid(*freeformInstances[lastCurveIndex]->getCurvePointer()) != typeid(BezierCurve)){
                freeformInstances.pop_back();
                freeformInstances.push_back(new FreeformInstance(new BezierCurve, *new float2));
            }
            freeformInstances[lastCurveIndex]->addControlPoint(mousePoint);
            currentCurveIndex = freeformInstances.size() - 1;
        }
        else if (aPressed){
            freeformInstances[currentCurveIndex]->addControlPoint(mousePoint);
        }
        else {
            if (freeformInstances.size() > 1){
                float minimumDistance = MAXFLOAT;
                int indexOfCurveWithClosestPoint = 0;
                int closestPointIndex = 0;
                float closestTValue = 0.0f;
                // Check if we're on any of the curves
                for (int i = 0; i < freeformInstances.size(); i++){
                    // Check control points first
                    std::vector<float2> controlPoints = freeformInstances[i]->getCurvePointer()->getControlPoints();
                    for (int j = 0; j < controlPoints.size(); j++){
                        float currentDistance = distance(mousePoint, controlPoints[j]);
                        if (currentDistance < minimumDistance){
                            minimumDistance = currentDistance;
                            closestPointIndex = j;
                            indexOfCurveWithClosestPoint = i;
                        }
                    }
                }
                if (
                    distance(
                             mousePoint,
                             freeformInstances[indexOfCurveWithClosestPoint]->getCurvePointer()->getControlPoints()[closestPointIndex]
                             ) < 0.023
                    ){
                    currentCurveIndex = indexOfCurveWithClosestPoint;
                    if (dPressed){
                        freeformInstances[currentCurveIndex]->removeControlPoint(closestPointIndex);
                        currentControlPointIndex = -1;
                    }
                    else {
                        currentControlPointIndex = closestPointIndex;
                    }
                }
                else {
                    for (int i = 0; i < freeformInstances.size(); i++){
                        for (double t = 0.0; t < 1.0; t += 0.01){
                            float2 tempPoint = freeformInstances[i]->getCurvePointer()->getPoint(t);
                            float currentDistance = distance(mousePoint, tempPoint);
                            if (currentDistance < minimumDistance){
                                minimumDistance = currentDistance;
                                indexOfCurveWithClosestPoint = i;
                                closestTValue = t;
                            }
                        }
                    }
                    if (
                        distance(
                                 mousePoint,
                                 freeformInstances[indexOfCurveWithClosestPoint]->getCurvePointer()->getPoint(closestTValue)
                                 ) < 0.018
                        ){
                        currentControlPointIndex = -1;
                        currentCurveIndex = indexOfCurveWithClosestPoint;
                    }
                }

            }
        }
        
        glutPostRedisplay();
        glutSwapBuffers();
    }
    
}


int main(int argc, char *argv[]) {
    
    time_t result = time(nullptr);
    int second = localtime(&result)->tm_sec;
    int minute = localtime(&result)->tm_min;
    int hour = localtime(&result)->tm_hour;
    
    BezierNumber counterSecondOnesDigit = BezierNumber(second / 10, 1.0f/4000.0f);
    BezierNumber counterSecondTensDigit = BezierNumber(second % 10, 1.0f/4000.0f);
    
    glutInit(&argc, argv);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(100, 100);
    glutInitDisplayMode(
                        GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    
    glutCreateWindow("To Build a Curve");
    
    freeformInstances.push_back(new FreeformInstance(new Polyline, *new float2));
    
    // Register event handlers here
    glutDisplayFunc(onDisplay);
    glutMouseFunc(onMouseClick);
    glutKeyboardUpFunc(onKeyUp);
    glutKeyboardFunc(onKeyDown);
    glutMotionFunc(onMouseMove);

    glutMainLoop();
    return 0;
}

