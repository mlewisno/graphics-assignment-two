#ifndef Graphics_Homework_Two_BezierNumber_h
#define Graphics_Homework_Two_BezierNumber_h

#pragma once
#include "float2.h"
#include "Freeform.h"

class BezierNumber: public Freeform
{
protected:
    std::vector<float> knots;
    int currentValue;
    float scale;
    float2 numberControlPoints [10][12];
public:
    BezierNumber(int value, float scaleValue){
        currentValue = value;
        scale = scaleValue;
        
        // The number 0
        numberControlPoints[0][0] = float2(159.0, 84.0);
        numberControlPoints[0][1] = float2(123.0, 158.0);
        numberControlPoints[0][2] = float2(131.0, 258.0);
        numberControlPoints[0][3] = float2(139.0, 358.0);
        numberControlPoints[0][4] = float2(167.0, 445.0);
        numberControlPoints[0][5] = float2(256.0, 446.0);
        numberControlPoints[0][6] = float2(345.0, 447.0);
        numberControlPoints[0][7] = float2(369.0, 349.0);
        numberControlPoints[0][8] = float2(369.0, 275.0);
        numberControlPoints[0][9] = float2(369.0, 201.0);
        numberControlPoints[0][10] = float2(365.0, 81.0);
        numberControlPoints[0][11] = float2(231.0, 75.0);
        
        // The number 1
        numberControlPoints[1][0] = float2(226.0, 99.0);
        numberControlPoints[1][1] = float2(230.0, 58.0);
        numberControlPoints[1][2] = float2(243.0, 43.0);
        numberControlPoints[1][3] = float2(256.0, 28.0);
        numberControlPoints[1][4] = float2(252.0, 100.0);
        numberControlPoints[1][5] = float2(253.0, 167.0);
        numberControlPoints[1][6] = float2(254.0, 234.0);
        numberControlPoints[1][7] = float2(254.0, 194.0);
        numberControlPoints[1][8] = float2(255.0, 303.0);
        numberControlPoints[1][9] = float2(256.0, 412.0);
        numberControlPoints[1][10] = float2(254.0, 361.0);
        numberControlPoints[1][11] = float2(255.0, 424.0);
        
        // The number 2
        numberControlPoints[2][0] = float2(152.0, 55.0);
        numberControlPoints[2][1] = float2(208.0, 26.0);
        numberControlPoints[2][2] = float2(271.0, 50.0);
        numberControlPoints[2][3] = float2(334.0, 74.0);
        numberControlPoints[2][4] = float2(360.0, 159.0);
        numberControlPoints[2][5] = float2(336.0, 241.0);
        numberControlPoints[2][6] = float2(312.0, 323.0);
        numberControlPoints[2][7] = float2(136.0, 454.0);
        numberControlPoints[2][8] = float2(120.0, 405.0);
        numberControlPoints[2][9] = float2(104.0, 356.0);
        numberControlPoints[2][10] = float2(327.0, 393.0);
        numberControlPoints[2][11] = float2(373.0, 414.0);
        
        // The number 3
        numberControlPoints[3][0] = float2(113.0, 14.0);
        numberControlPoints[3][1] = float2(267.0, 17.0);
        numberControlPoints[3][2] = float2(311.0, 107.0);
        numberControlPoints[3][3] = float2(355.0, 197.0);
        numberControlPoints[3][4] = float2(190.0, 285.0);
        numberControlPoints[3][5] = float2(182.0, 250.0);
        numberControlPoints[3][6] = float2(174.0, 215.0);
        numberControlPoints[3][7] = float2(396.0, 273.0);
        numberControlPoints[3][8] = float2(338.0, 388.0);
        numberControlPoints[3][9] = float2(280.0, 503.0);
        numberControlPoints[3][10] = float2(110.0, 445.0);
        numberControlPoints[3][11] = float2(93.0, 391.0);
        
        // The number 4
        numberControlPoints[4][0] = float2(249.0, 230.0);
        numberControlPoints[4][1] = float2(192.0, 234.0);
        numberControlPoints[4][2] = float2(131.0, 239.0);
        numberControlPoints[4][3] = float2(70.0, 244.0);
        numberControlPoints[4][4] = float2(142.0, 138.0);
        numberControlPoints[4][5] = float2(192.0, 84.0);
        numberControlPoints[4][6] = float2(242.0, 30.0);
        numberControlPoints[4][7] = float2(283.0, -30.0);
        numberControlPoints[4][8] = float2(260.0, 108.0);
        numberControlPoints[4][9] = float2(237.0, 246.0);
        numberControlPoints[4][10] = float2(246.0, 435.0);
        numberControlPoints[4][11] = float2(247.0, 438.0);
        
        // The number 5
        numberControlPoints[5][0] = float2(226.0, 42.0);
        numberControlPoints[5][1] = float2(153.0, 44.0);
        numberControlPoints[5][2] = float2(144.0, 61.0);
        numberControlPoints[5][3] = float2(135.0, 78.0);
        numberControlPoints[5][4] = float2(145.0, 203.0);
        numberControlPoints[5][5] = float2(152.0, 223.0);
        numberControlPoints[5][6] = float2(159.0, 243.0);
        numberControlPoints[5][7] = float2(351.0, 165.0);
        numberControlPoints[5][8] = float2(361.0, 302.0);
        numberControlPoints[5][9] = float2(371.0, 439.0);
        numberControlPoints[5][10] = float2(262.0, 452.0);
        numberControlPoints[5][11] = float2(147.0, 409.0);
        
        // The number 6
        numberControlPoints[6][0] = float2(191.0, 104.0);
        numberControlPoints[6][1] = float2(160.0, 224.0);
        numberControlPoints[6][2] = float2(149.0, 296.0);
        numberControlPoints[6][3] = float2(138.0, 368.0);
        numberControlPoints[6][4] = float2(163.0, 451.0);
        numberControlPoints[6][5] = float2(242.0, 458.0);
        numberControlPoints[6][6] = float2(321.0, 465.0);
        numberControlPoints[6][7] = float2(367.0, 402.0);
        numberControlPoints[6][8] = float2(348.0, 321.0);
        numberControlPoints[6][9] = float2(329.0, 240.0);
        numberControlPoints[6][10] = float2(220.0, 243.0);
        numberControlPoints[6][11] = float2(168.0, 285.0);
        
        // The number 7
        numberControlPoints[7][0] = float2(168.0, 34.0);
        numberControlPoints[7][1] = float2(245.0, 42.0);
        numberControlPoints[7][2] = float2(312.0, 38.0);
        numberControlPoints[7][3] = float2(379.0, 34.0);
        numberControlPoints[7][4] = float2(305.0, 145.0);
        numberControlPoints[7][5] = float2(294.0, 166.0);
        numberControlPoints[7][6] = float2(283.0, 187.0);
        numberControlPoints[7][7] = float2(243.0, 267.0);
        numberControlPoints[7][8] = float2(231.0, 295.0);
        numberControlPoints[7][9] = float2(219.0, 323.0);
        numberControlPoints[7][10] = float2(200.0, 388.0);
        numberControlPoints[7][11] = float2(198.0, 452.0);
        
        // The number 8
        numberControlPoints[8][0] = float2(336.0, 184.0);
        numberControlPoints[8][1] = float2(353.0, 52.0);
        numberControlPoints[8][2] = float2(240.0, 43.0);
        numberControlPoints[8][3] = float2(127.0, 34.0);
        numberControlPoints[8][4] = float2(143.0, 215.0);
        numberControlPoints[8][5] = float2(225.0, 247.0);
        numberControlPoints[8][6] = float2(307.0, 279.0);
        numberControlPoints[8][7] = float2(403.0, 427.0);
        numberControlPoints[8][8] = float2(248.0, 432.0);
        numberControlPoints[8][9] = float2(93.0, 437.0);
        numberControlPoints[8][10] = float2(124.0, 304.0);
        numberControlPoints[8][11] = float2(217.0, 255.0);
        
        // The number 9
        numberControlPoints[9][0] = float2(323.0, 6.0);
        numberControlPoints[9][1] = float2(171.0, 33.0);
        numberControlPoints[9][2] = float2(151.0, 85.0);
        numberControlPoints[9][3] = float2(131.0, 137.0);
        numberControlPoints[9][4] = float2(161.0, 184.0);
        numberControlPoints[9][5] = float2(219.0, 190.0);
        numberControlPoints[9][6] = float2(277.0, 196.0);
        numberControlPoints[9][7] = float2(346.0, 149.0);
        numberControlPoints[9][8] = float2(322.0, 122.0);
        numberControlPoints[9][9] = float2(298.0, 95.0);
        numberControlPoints[9][10] = float2(297.0, 365.0);
        numberControlPoints[9][11] = float2(297.0, 448.0);
        
        for (int i = 0; i < 10; i ++){
            for (int j = 0; j < 12; j ++){
                numberControlPoints[i][j].x *= scale;
                numberControlPoints[i][j].y *= -scale;
            }
        }

        for (int i = 0; i < 12; i ++){
            controlPoints.push_back(numberControlPoints[value][i]);
        }
    }
    
    static double bernstein(int i, int n, double t) {
        if(n == 1) {
            if(i == 0) return 1-t;
            if(i == 1) return t;
            return 0;
        }
        if(i < 0 || i > n) return 0;
        return (1 - t) * bernstein(i,   n-1, t)
        +      t  * bernstein(i-1, n-1, t);
    }
    
    float2 getPoint(float t)
    {
        float2 r(0.0, 0.0);
        float2 newPoint = float2(0, 0);
        int size = (int)controlPoints.size();
        
        for (int i = 0; i < size; i ++){
            double weight = bernstein(i, size - 1, t);
            
            r = controlPoints[i];
            r = r * weight;
            
            newPoint += r;
        }
        return newPoint;
    }
    
    void addControlPoint(float2 newPoint){
    }
    
    float2 getDerivative(float t){
        return float2(0.0, 0.0);
    }
    
    float2 getSecondDerivative(float t){
        return float2(0.0, 0.0);
    }
    
    void removeControlPoint(int index){
    }
    
    void shiftControlPoints(float deltaTime, int time, float period){
        
        currentValue = time;
        
        // Figure out between which two numbers we need to be
        int nextValue = (currentValue + 1) % 10;
                
        // Build linear functions between each set of contorl points
        for (int i = 0; i < 12; i ++){
            float2 beginningControlPoint = numberControlPoints[currentValue][i];
            float2 currentControlPoint = controlPoints[i];
            float2 targetControlPoint = numberControlPoints[nextValue][i];
            
            float2 differenceVector = targetControlPoint - beginningControlPoint;
            
            float2 newControlPoint = currentControlPoint + float2(differenceVector.x * (deltaTime ), differenceVector.y * (deltaTime ));
            controlPoints[i] = newControlPoint;
        }
    }
    
    void draw(float deltaTime, float time, float period, bool update){
        
        if (update){
            shiftControlPoints(deltaTime, time, period);
        }
        
        glBegin(GL_LINE_STRIP);
        for (double i = 0.0; i < 1.0; i += PRECISION){
            float2 tempPoint = getPoint(i);
            glVertex2d(tempPoint.x, tempPoint.y);
        }
        glEnd();
    }
};

#endif
