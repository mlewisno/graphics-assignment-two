#ifndef Graphics_Homework_Two_FreeformInstance_h
#define Graphics_Homework_Two_FreeformInstance_h

#pragma once
#include "float2.h"
#include "Freeform.h"

class FreeformInstance
{
protected:
    Freeform* curve; // the geometric model,
    int numberOfControlPoints = 0;
    bool drawPolygon = false;
public:
    FreeformInstance(Freeform* curve, float2 position)
    :curve(curve) {}
    void draw()
    {
        curve->draw();
        if (drawPolygon){
            curve->drawPolygon();
        }
    }
    void translatePosition(float2 positionChange)
    {
        curve->updateAllControlPoints(positionChange);
    }
    void addControlPoint(float2 newPoint)
    {
        curve->addControlPoint(newPoint);
        numberOfControlPoints ++;
    }
    void drawControlPoints(int currentlySelectedIndex){
        curve->drawControlPoints(currentlySelectedIndex);
    }
    Freeform *getCurvePointer(){
        return curve;
    }
    int getNumberOfControlPoints(){
        return numberOfControlPoints;
    }
    void updateControlPoint(int pointIndex, float2 newPoint){
        curve->updateControlPoint(pointIndex, newPoint);
    }
    void removeControlPoint(int index){
        curve->removeControlPoint(index);
        numberOfControlPoints --;
    }
    void updatePolygonStatus(bool willDrawPolygon){
        drawPolygon = willDrawPolygon;
    }
};


#endif
