#ifndef Graphics_Homework_Two_Heart_h
#define Graphics_Homework_Two_Heart_h

#pragma once
#include "float2.h"
#include "Curve.h"

class Heart: public Curve{
public:
    float2 startPoint;
    float scale = 20;
    
    Heart(float2 startPoint, float scale){
        this->startPoint = startPoint;
    }
    
    float2 getPoint(float t){
        float time = t * 2 * M_PI;
        return float2(startPoint.x + (16 * sinf(time) * sinf(time) * sinf(time))/scale, startPoint.y + (13 * cosf(time) - 5 * cosf(2*time) - 2*cosf(3*time) - cosf(4*time))/scale);
    }
    
    float2 getDerivative(float t){
        float time = t * 2 * M_PI;
        return float2(startPoint.x + (48 * sinf(time) * sinf(time) * cosf(time))/scale, startPoint.y + (-13 * sinf(time) + 10 * sinf(2*time) + 6 *sinf(3*time) + sinf(4*time))/scale);
    }
    
    float2 getSecondDerivative(float t){
        float time = t * 2 * M_PI;
        return float2(startPoint.x + (16 * sinf(time) * sinf(time) * sinf(time))/scale, startPoint.y + (13 * cos(time) - 5 * cos(2*time) - 2*cos(3*time) - cos(4*time))/scale);
    }
    
};


#endif
