#ifndef Graphics_Homework_Two_Freeform_h
#define Graphics_Homework_Two_Freeform_h

#pragma once
#include "float2.h"
#include "Curve.h"

class Freeform: public Curve
{
protected:
    std::vector<float2> controlPoints;
public:
    virtual float2 getPoint(float t)=0;
    virtual void addControlPoint(float2 p)
    {
        controlPoints.push_back(p);
    }
    void drawPolygon(){
        glColor3f(0.1, 0.1, 0.1);
        glBegin(GL_POLYGON);
        for (double i = 0.0; i < 1.0; i += PRECISION){
            float2 tempPoint = getPoint(i);
            glVertex2d(tempPoint.x, tempPoint.y);
        }
        glEnd();
    }
    void drawControlPoints(int currentlySelectedIndex){
        int size = (int)controlPoints.size();
        
        glPointSize(10);
        glBegin(GL_POINTS);
        
        for (int i = 0; i < size; i++){
            if (currentlySelectedIndex == i){
                glColor3d(1.0, 1.0, 1.0);
            }
            else {
                glColor3d(0, 1.0, 0);
            }
            float2 currentPoint = controlPoints[i];
            glVertex2f(currentPoint.x, currentPoint.y);
        }
        
        glEnd();
        
    }
    void updateControlPoint(int pointIndex, float2 newPoint){
        controlPoints[pointIndex] = newPoint;
    }
    void updateAllControlPoints(float2 offSet){
        int size = (int)controlPoints.size();
        
        for (int i = 0; i < size; i++){
            controlPoints[i].x += offSet.x;
            controlPoints[i].y += offSet.y;
        }
    }
    virtual void removeControlPoint(int index)=0;
    
    std::vector<float2> getControlPoints(){
        return controlPoints;
    }
    
};


#endif
