#ifndef Graphics_Homework_Two_LagrangeCurve_h
#define Graphics_Homework_Two_LagrangeCurve_h

#pragma once
#include "float2.h"
#include "Freeform.h"

class LagrangeCurve: public Freeform
{
protected:
    std::vector<float> knots;
    
public:
    
    float getWeight(int i, float t) {
        float Li = 1.0f;
        for(int j = 0; j < controlPoints.size(); j++)
            if (j != i) Li *= (t - knots[j])/(knots[i] - knots[j]);
        return Li;
    }
    
    float2 getPoint(float t)
    {
        
        
        float2 r(0.0, 0.0);
        
        float2 newPoint = float2(0, 0);
        
        int size = (int)controlPoints.size();
        
        for (int i = 0; i < size; i ++){
            float weight = getWeight(i, t);
            //printf("The weight is %f", weight);
            
            r = controlPoints[i];
            r = r * weight;
            
            newPoint += r;
        }
        
        //printf("The point is (%f, %f)", newPoint.x, newPoint.y);
        
        return newPoint;
    }
    
    void addControlPoint(float2 newPoint){
        int numKnots = controlPoints.size();
        
        knots.push_back(1);
        
        for (float i = 0; i < numKnots; i++){
            knots[i] = (i * 1/numKnots);
        }
        
        controlPoints.push_back(newPoint);
    }
    
    float2 getDerivative(float t){
        return float2(0.0, 0.0);
    }
    
    float2 getSecondDerivative(float t){
        return float2(0.0, 0.0);
        
    }
    
    void removeControlPoint(int index){
        controlPoints.erase(controlPoints.begin() + index);
        knots.clear();
        
        if (controlPoints.size() > 1){
            int numKnots = controlPoints.size() - 1;
            for (float i = 0; i <= numKnots; i++){
                knots.push_back(i * 1.0f/numKnots);
            }
            
        }
        
    }
};

#endif
